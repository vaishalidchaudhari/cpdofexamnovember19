package com.agiletestingalliance.verify;

import static org.junit.Assert.*;
import java.io.*;
import org.junit.Test;
import com.agiletestingalliance.*;
 
import org.junit.Before;

public class CPDoFWebAppTest {


    @Test
    public void testMinMax() throws Exception {
        int returnValue = new MinMax().minMaxInt(10,20);
        assertEquals("Problem with minMaxInt funtion:",20,returnValue);
    }

    @Test
    public void testMinMax1() throws Exception {
        int returnValue = new MinMax().minMaxInt(20,5);
        assertEquals("Problem with minMaxInt funtion:",20,returnValue);
    }


    @Test
    public void testdesc() throws Exception {

        String value = "CP-DOF certification program";

        String strValue= new AboutCPDOF().desc();
        assertTrue("Problem with desc function:", strValue.contains(value));
        
    }


    @Test
    public void testdur() throws Exception {

        String value = "CP-DOF is designed specifically for corporates and working professionals alike.";

        String strValue= new Duration().dur();
        assertTrue("Problem with desc function:", strValue.contains(value));
        
    }

    @Test
    public void testdesc1() throws Exception {

        String value = "DevOps is about transformation";

        String strValue= new Usefulness().desc();
        assertTrue("Problem with desc function:", strValue.contains(value));
        
    }
}
